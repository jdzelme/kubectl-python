#!/usr/bin/env python3
import subprocess
import os


def main():
    set_user_env()
    d = {}
    while len(d) == 0:
        print('Filter:')
        filter_string = input("");
        if filter_string == "0":
            raise SystemExit
        cmd = ["kubectl", "get", "pods"]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        c = 1
        while p.poll() is None:
            line = p.stdout.readline()
            if (len(filter_string) and filter_string in line) or (len(line) and filter_string == ""):
                a = line.split()
                print(str(c) + ": " + line)
                d[c] = [a[0], line]
                c += 1
    user_choice = ask_user(d)
    choice_ = d[user_choice]
    command = "kubectl exec -it " + choice_[0] + " -c app /bin/bash"
    os.system("gnome-terminal -e 'bash -c \"" + command + ";bash\"'")


def set_user_env():
    print('1: Live')
    print('2: Serverless')
    print('3: Staging')
    print('4: US')
    envs = {1: 'Live', 2: 'Serverless', 3: 'Staging', 4: 'US'}
    env_choice = 0
    while env_choice not in envs:
        print('Select a ENV number')
        txt = input("")
        env_choice = int(txt)
        if env_choice == 0:
            raise SystemExit
    print(envs[env_choice] + ' selected')
    cmd = ["kubectl", "config", "use-context", envs[env_choice]]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)


def ask_user(d):
    user_choice = 0
    while user_choice not in d:
        print('Select a pod number to connect to (0 to exit)')
        txt = input("")
        user_choice = int(txt)
        if user_choice == 0:
            raise SystemExit
    return user_choice


if __name__ == "__main__":
    main()
